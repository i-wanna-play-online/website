$(() => {
	getGames()
	.then(showGames)
	.catch(console.error);
	showVersion()
	.catch(console.error);
	$("#refresh").click(() => {
		getGames()
		.then(showGames)
		.catch(console.error);
		showVersion()
		.catch(console.error);
	});
});

const showVersion = async () => {
	const version = await getVersion();
	$("#version").text(`If you have a version below ${version} (see README.txt, last release on Sunday, February 27 2022), you are not up to date and should download the latest version for everything to work! Otherwise it might still work, but you might have some issues.`);
	$("a.button").css("display", "inline-block").attr("href", `iwpo ${version}.zip`).text(`DOWNLOAD THE TOOL ver${version}`);
}

const getVersion = () => new Promise((resolve, reject) => {
	$.post("/version", data => {
		if(data.success)
			resolve(data.version);
		else
			reject();
	});
});

const getGames = () => new Promise((resolve, reject) => {
	$.post("/getGames", data => {
		if(data.success)
			resolve(data.games.sort((game1, game2) => game2.players-game1.players));
		else
			reject(data.error);
	});
});

const getGameMetadata = gameName => new Promise((resolve, reject) => {
	$.ajax({
		type: "POST",
		url: "/getGameMetadata",
		contentType: "application/json",
		data: JSON.stringify({
			gameName: gameName
		})
	}).done(data => {
		if(data.success)
			resolve(data.metadata);
		else
			reject(data.error);
	}).fail(reject);
});

const showGames = games => {
	$(".lds-ring").show();
	$("#refresh").hide();
	$("#content").html("");
	if(games.length > 0){
		const numberOfPlayers = games.reduce((t, g) => t+g.players, 0);
		$("#content").append($("<p>").text(`${numberOfPlayers} player${numberOfPlayers > 1 ? "s" : ""} ${numberOfPlayers > 1 ? "are" : "is"} online right now!`));
		for(const game of games){
			if(game.hasPassword)
				continue;
			const div = $("<div>").addClass("game");
			const title = $("<p>").text(game.name).addClass("title");
			const img = $("<img>").attr({
				src: "/loading.png",
				id: "screenshot"
			}).addClass("screenshot");
			const players = $("<span>").html(`&nbsp;Online: ${game.players}&nbsp;&nbsp;&nbsp;`);
			const download = $("<span>").text("Looking for download...");
			const description = $("<p>").addClass("description")
			.append($("<i>").attr("class", "fa fa-user"))
			.append(players)
			.append($("<i>").attr("class", "fa fa-download"))
			.append($("<span>").html("&nbsp;"))
			.append(download);
			div.append(title);
			div.append(img);
			div.append(description);
			$("#content").append(div);
			(async () => {
				const metadata = await getGameMetadata(game.name);
				title.text(metadata.name);
				if(metadata.download.length > 0){
					const link = $("<a>").attr({
						target: "_blank",
						href: metadata.download
					}).text("Download");
					download.html(link);
				}else{
					download.text("No download found");
				}
				if(metadata.screenshot.length > 0)
					img.attr("src", metadata.screenshot);
				else
					img.attr("src", "https://images.fonearena.com/blog/wp-content/uploads/2012/04/noScreenShot.jpg");
			})()
			.catch(console.error);
		}
	}else{
		const noGame = $("<p>").text("Sorry, no game is played online for the moment.").addClass("no-game");
		$("#content").append(noGame);
	}
	$(".lds-ring").hide();
	$("#refresh").show();
}
