import express from "express";
import { PORT } from "./env";
import { logger } from "./logger";
import { router } from "./router";
import { PUBLIC_DIR } from "./public";

const app = express();

app.use(express.static(PUBLIC_DIR));
app.use(express.json());
app.use("/", router);

app.use((_, res) => {
	res.json({
		success: false,
		error: "Invalid route"
	});
});

app.use((err: Error, _: any, res: any, __: any) => {
	logger.error(err);
	res.json({
		success: false,
		error: err.message
	});
});

app.listen(PORT, () => {
	logger.info(`Server started on port ${PORT}.`);
});
