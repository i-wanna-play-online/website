export interface Metadata {
	name: string;
	download: string;
	screenshot: string;
}

export interface Data {
	success: boolean;
	error: string;
	metadata: Metadata;
}

export interface Game {
	id: number;
	name: string;
}
