import dotenv from "dotenv";

dotenv.config();

export const PORT = parseInt(process.env.PORT ?? "Missing port");
export const IWPO_API = process.env.IWPO_API ?? "Missing IWPO API";
export const DELFRUIT_API = process.env.DELFRUIT_API ?? "Missing DelFruit API";
export const DELFRUIT_API_KEY = process.env.DELFRUIT_API_KEY ?? "Missing DelFruit API key";
