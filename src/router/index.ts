import express from "express";
import { DELFRUIT_API, DELFRUIT_API_KEY, IWPO_API } from "../env";
import { Data, Game } from "../types";
import ash from "express-async-handler";
import fs from "fs/promises";
import { PUBLIC_DIR } from "../public";

export const router = express.Router();

const getVersion = async function () {
	const files = await fs.readdir(PUBLIC_DIR);
	const file = files.find(file => file.slice(0, 5) == "iwpo ");
	if (!file) throw new Error("No version found");
	return file.slice(5, file.length - 4);
}

router.post("/version", ash(async (_, res) => {
	const version = await getVersion();
	res.json({
		version: version,
		success: true
	});
}));

router.post("/getGames", ash(async (_, res) => {
	const response = await fetch(IWPO_API);
	const body = await response.json();
	const data = {
		success: true,
		error: "",
		games: body.games
	};
	res.json(data);
}));

router.post("/getGameMetadata", ash(async (req, res) => {
	const data: Data = {
		success: true,
		error: "",
		metadata: {
			name: req.body
				.gameName
				.replace(/(v|ver)\.?\s?\d+(\.\d+)*/gi, "")
				.replace(/\d+(\.\d+)+/gi, "")
				.trim(),
			download: "",
			screenshot: ""
		},
	};
	const response = await fetch(DELFRUIT_API, {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			api_key: DELFRUIT_API_KEY,
			method: "search",
			title: data.metadata.name
		})
	});
	interface Response {
		success: boolean;
		games: Game[];
	}
	const json: Response = await response.json();
	if (!json.success) throw new Error("Failed to get game data");
	if (json.games.length == 0) throw new Error("No games found");
	const games = json.games.map(game => ({
		...game,
		name: game.name.replace(/\(共同開発\)/g, ""),
	}));
	games.sort((a, b) => a.name.length - b.name.length);
	const game = games[0];
	if (!game) {
		res.json(data);
		return;
	}
	data.metadata.name = game.name;
	data.metadata.download = `https://delicious-fruit.com/ratings/game_details.php?id=${game.id}`;
	const response2 = await fetch(data.metadata.download);
	const download = await response2.text();
	const match = download.match(/screenshots\/([A-Za-z0-9\_]*)/);
	if (match != null)
		data.metadata.screenshot = `https://delicious-fruit.com/ratings/screenshots/${match[1]}`;
	res.json(data);
}));
